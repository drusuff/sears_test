(function() {
    'use strict';

    angular
    .module('MovieApp')
    .factory('MovieService', MovieService);

    MovieService.$inject = ['$http'];

    function MovieService($http) {

        return {
            saveMovie: saveMovie
        };
        
        function saveMovie(movie){

            return $http.post('/api/movie', movie)//Change to movie.php for 200
                .then(saveMovieComplete)
                .catch(saveMovieFailed);
            
            function saveMovieComplete (response){
                response.status = 201;
                return response;
            }

            function saveMovieFailed(response){
                response.status = 400;
                return response;
            }            
        }        
    }
})();