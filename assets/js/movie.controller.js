(function() {
    'use strict';

    angular
    .module('MovieApp')
    .controller('MovieController', MovieController);

    MovieController.$inject = ['MovieService'];

    function MovieController(MovieService) {

        var vm = this;

        vm.movie = {};
        vm.movie.released = '1';        
        vm.saveMovie = saveMovie;

        function saveMovie(){
            
            return MovieService.saveMovie(vm.movie).then(function (response){                 

                if (response.status === 201){
                    alert("Movie Saved!")
                } else {
                    console.log(response);
                } 
            })
        }
    }
})();