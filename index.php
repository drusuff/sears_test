<!DOCTYPE html>
<html>
<head>
    
    <title>Sears Coding Project</title>

    <script src="//code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" crossorigin="anonymous">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" crossorigin="anonymous">

    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>

    <link rel="stylesheet" href="/assets/css/main.css">    

</head>
<body class="container" ng-app="MovieApp" ng-controller="MovieController as ctrl" ng-cloak>

    <form class="row form-horizontal" name="movieForm">

        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title:</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="title" name="title" ng-model="ctrl.movie.title" placeholder="Add Title" required>
            </div>            
        </div>

        <div class="form-group">
            <label for="year" class="col-sm-2 control-label">Year:</label>
            <div class="col-sm-3">
                <input type="number" class="form-control" id="year" name="year" ng-model="ctrl.movie.year" placeholder="Add Year" ng-minlength="4" ng-maxlength="4" required>
            </div>
        </div>

        <div class="form-group">
            <label for="director" class="col-sm-2 control-label">Director:</label>
            <div class="col-sm-3">
                <input type="text" pattern="[a-zA-Z]+" ng-pattern-restrict class="form-control" id="director" name="director" ng-model="ctrl.movie.director" placeholder="Add Director"required>
            </div>
        </div>

        <div class="form-group">
            <label for="genre" class="col-sm-2 control-label">Genre:</label>
            <div class="col-sm-3">
                <select id="genre" name="genre" class="form-control" ng-model="ctrl.movie.genre" required>
                    <option value="" disabled>Select Genre</option>
                    <option value="1">Action</option>
                    <option value="2">Sci-Fi</option>
                    <option value="3">Comedy</option>
                    <option value="4">Drama4</option>
                    <option value="5">Horror5</option>
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label for="released" class="col-sm-2 control-label">Released:</label>
            <div class="col-sm-3">
                <select id="released" name="released" class="form-control" ng-model="ctrl.movie.released">
                    <option value="0">No</option>
                    <option value="1">Yes</option>                   
                </select>
            </div>
        </div>

        <div>
            <button type="button" class="btn btn-success" ng-click="ctrl.saveMovie()" ng-disabled="movieForm.$invalid">Save Movie</button>
        </div>
    </form>

    <script src="/assets/js/movie.app.module.js"></script>
    <script src="/assets/js/movie.service.js"></script>
    <script src="/assets/js/movie.controller.js"></script>

</body>
</html>